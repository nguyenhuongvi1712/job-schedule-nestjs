import { Injectable } from '@nestjs/common';
import { AgendaService } from 'nestjs-agenda';
const { execute } = require('@getvim/execute');
const dotenv = require('dotenv');
dotenv.config();
const username = process.env.DB_USERNAME;
const database = process.env.DB_NAME;
const date = new Date();
const currentDate = `${date.getFullYear()}.${
  date.getMonth() + 1
}.${date.getDate()}.${date.getHours()}.${date.getMinutes()}`;
const fileName = `database-backup-${currentDate}.tar`;

@Injectable()
export class FooService {
  constructor(private readonly agenda: AgendaService) {
    // define a job, more details: [Agenda documentation](https://github.com/agenda/agenda)
    this.agenda.define(
      'TEST_JOB',
      { lockLifetime: 10000 },
      this.testJob.bind(this),
    );
    this.agenda.define('DB_BACKUP', { lockLifetime: 10000 }, this.backup);

    // schedule a job
    this.agenda.every('0 59 23 * * *', ['TEST_JOB', 'DB_BACKUP'], {});
  }

  private async testJob(job: any, done: any): Promise<void> {
    console.log('a job');
    await job.remove();
    done();
  }

  private async backup(): Promise<any> {
    execute(
      `pg_dump -U ${username} -d ${database} -f db-backup/${fileName} -F t`,
    )
      .then(async () => {
        console.log(
          `pg_dump -U ${username} -d ${database} -f db-backup / ${fileName} -F t`,
        );
      })
      .catch((err) => {
        console.log(err);
      });
  }
}

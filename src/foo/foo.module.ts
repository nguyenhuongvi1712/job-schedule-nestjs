import { AgendaModule } from 'nestjs-agenda';
import { Module } from '@nestjs/common';
import { FooService } from './foo.service';
const connectionString = 'mongodb://127.0.0.1:27017/schedule-tasks';
@Module({
  imports: [
    AgendaModule.register({
      db: { address: connectionString, collection: 'jobs' },
    }),
  ],
  providers: [FooService],
})
export class FooModule {}
